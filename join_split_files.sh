#!/bin/bash

cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat odm/lib64/librelight_only.so.* 2>/dev/null >> odm/lib64/librelight_only.so
rm -f odm/lib64/librelight_only.so.* 2>/dev/null
cat vendor/etc/camera/8D84710A.* 2>/dev/null >> vendor/etc/camera/8D84710A
rm -f vendor/etc/camera/8D84710A.* 2>/dev/null
cat vendor/etc/nn/ot/neuron_detection.mdla3_5.* 2>/dev/null >> vendor/etc/nn/ot/neuron_detection.mdla3_5
rm -f vendor/etc/nn/ot/neuron_detection.mdla3_5.* 2>/dev/null
cat vendor/lib64/mt6886/libaibc_tuning.so.* 2>/dev/null >> vendor/lib64/mt6886/libaibc_tuning.so
rm -f vendor/lib64/mt6886/libaibc_tuning.so.* 2>/dev/null
cat vendor/lib64/mt6886/libaibc_tuning_p2.so.* 2>/dev/null >> vendor/lib64/mt6886/libaibc_tuning_p2.so
rm -f vendor/lib64/mt6886/libaibc_tuning_p2.so.* 2>/dev/null
cat vendor/lib64/mt6886/libaiawb_moon_model.so.* 2>/dev/null >> vendor/lib64/mt6886/libaiawb_moon_model.so
rm -f vendor/lib64/mt6886/libaiawb_moon_model.so.* 2>/dev/null
cat vendor/lib64/librelight_only.so.* 2>/dev/null >> vendor/lib64/librelight_only.so
rm -f vendor/lib64/librelight_only.so.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat product/data-app/MiShop/MiShop.apk.* 2>/dev/null >> product/data-app/MiShop/MiShop.apk
rm -f product/data-app/MiShop/MiShop.apk.* 2>/dev/null
cat product/data-app/SmartHome/SmartHome.apk.* 2>/dev/null >> product/data-app/SmartHome/SmartHome.apk
rm -f product/data-app/SmartHome/SmartHome.apk.* 2>/dev/null
cat product/data-app/MIUINotes/MIUINotes.apk.* 2>/dev/null >> product/data-app/MIUINotes/MIUINotes.apk
rm -f product/data-app/MIUINotes/MIUINotes.apk.* 2>/dev/null
cat product/data-app/MIpay/MIpay.apk.* 2>/dev/null >> product/data-app/MIpay/MIpay.apk
rm -f product/data-app/MIpay/MIpay.apk.* 2>/dev/null
cat product/data-app/Gallery_T_CN/Gallery_T_CN.apk.* 2>/dev/null >> product/data-app/Gallery_T_CN/Gallery_T_CN.apk
rm -f product/data-app/Gallery_T_CN/Gallery_T_CN.apk.* 2>/dev/null
cat product/data-app/MiMediaEditor/MiMediaEditor.apk.* 2>/dev/null >> product/data-app/MiMediaEditor/MiMediaEditor.apk
rm -f product/data-app/MiMediaEditor/MiMediaEditor.apk.* 2>/dev/null
cat product/data-app/MIUIYoupin/MIUIYoupin.apk.* 2>/dev/null >> product/data-app/MIUIYoupin/MIUIYoupin.apk
rm -f product/data-app/MIUIYoupin/MIUIYoupin.apk.* 2>/dev/null
cat product/data-app/MIUIVideo/MIUIVideo.apk.* 2>/dev/null >> product/data-app/MIUIVideo/MIUIVideo.apk
rm -f product/data-app/MIUIVideo/MIUIVideo.apk.* 2>/dev/null
cat product/data-app/MiuiScanner/MiuiScanner.apk.* 2>/dev/null >> product/data-app/MiuiScanner/MiuiScanner.apk
rm -f product/data-app/MiuiScanner/MiuiScanner.apk.* 2>/dev/null
cat product/data-app/Health/Health.apk.* 2>/dev/null >> product/data-app/Health/Health.apk
rm -f product/data-app/Health/Health.apk.* 2>/dev/null
cat product/data-app/MIUIMusicT/MIUIMusicT.apk.* 2>/dev/null >> product/data-app/MIUIMusicT/MIUIMusicT.apk
rm -f product/data-app/MIUIMusicT/MIUIMusicT.apk.* 2>/dev/null
cat product/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk.* 2>/dev/null >> product/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk
rm -f product/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk.* 2>/dev/null
cat product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> product/priv-app/MiuiCamera/MiuiCamera.apk
rm -f product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat product/priv-app/MIUIBrowser/MIUIBrowser.apk.* 2>/dev/null >> product/priv-app/MIUIBrowser/MIUIBrowser.apk
rm -f product/priv-app/MIUIBrowser/MIUIBrowser.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/priv-app/MIUIAICR/MIUIAICR.apk.* 2>/dev/null >> product/priv-app/MIUIAICR/MIUIAICR.apk
rm -f product/priv-app/MIUIAICR/MIUIAICR.apk.* 2>/dev/null
cat product/app/VoiceAssistAndroidT/VoiceAssistAndroidT.apk.* 2>/dev/null >> product/app/VoiceAssistAndroidT/VoiceAssistAndroidT.apk
rm -f product/app/VoiceAssistAndroidT/VoiceAssistAndroidT.apk.* 2>/dev/null
cat product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null >> product/app/WebViewGoogle64/WebViewGoogle64.apk
rm -f product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null
cat product/app/CarWith/CarWith.apk.* 2>/dev/null >> product/app/CarWith/CarWith.apk
rm -f product/app/CarWith/CarWith.apk.* 2>/dev/null
cat product/app/MiLinkCirculate3/MiLinkCirculate3.apk.* 2>/dev/null >> product/app/MiLinkCirculate3/MiLinkCirculate3.apk
rm -f product/app/MiLinkCirculate3/MiLinkCirculate3.apk.* 2>/dev/null
cat product/app/MiGameService_MTK/MiGameService_MTK.apk.* 2>/dev/null >> product/app/MiGameService_MTK/MiGameService_MTK.apk
rm -f product/app/MiGameService_MTK/MiGameService_MTK.apk.* 2>/dev/null
cat product/app/MiuiBiometric/MiuiBiometric.apk.* 2>/dev/null >> product/app/MiuiBiometric/MiuiBiometric.apk
rm -f product/app/MiuiBiometric/MiuiBiometric.apk.* 2>/dev/null
cat product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null >> product/app/TrichromeLibrary64/TrichromeLibrary64.apk
rm -f product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null
