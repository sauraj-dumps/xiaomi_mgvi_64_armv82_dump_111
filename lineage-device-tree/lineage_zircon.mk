#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from zircon device
$(call inherit-product, device/xiaomi/zircon/device.mk)

PRODUCT_DEVICE := zircon
PRODUCT_NAME := lineage_zircon
PRODUCT_BRAND := Redmi
PRODUCT_MODEL := 23090RA98C
PRODUCT_MANUFACTURER := xiaomi

PRODUCT_GMS_CLIENTID_BASE := android-xiaomi

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="missi_phone_cn-user 13 TP1A.220624.014 V14.0.7.0.TNOCNXM release-keys"

BUILD_FINGERPRINT := Redmi/zircon/zircon:13/TP1A.220624.014/V14.0.7.0.TNOCNXM:user/release-keys
